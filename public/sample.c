#include <stdio.h>
#include <stdlib.h>
#include <fenv.h>
#include <math.h>

/*
$ gcc -frounding-math sample.c -lm
$ ./a.out 2.0
FE_TONEAREST:  sqrt(x)=0x1.6a09e667f3bcdp+0
FE_TOWARDZERO: sqrt(x)=0x1.6a09e667f3bccp+0
FE_UPWARD:     sqrt(x)=0x1.6a09e667f3bcdp+0
FE_DOWNWARD:   sqrt(x)=0x1.6a09e667f3bccp+0
*/

int
main (int argc, char *argv[])
{
  double x = atof (argv[1]);
  fesetround (FE_TONEAREST);
  printf ("FE_TONEAREST:  sqrt(x)=%la\n", sqrt (x));
  fesetround (FE_TOWARDZERO);
  printf ("FE_TOWARDZERO: sqrt(x)=%la\n", sqrt (x));
  fesetround (FE_UPWARD);
  printf ("FE_UPWARD:     sqrt(x)=%la\n", sqrt (x));
  fesetround (FE_DOWNWARD);
  printf ("FE_DOWNWARD:   sqrt(x)=%la\n", sqrt (x));
  return 0;
}
