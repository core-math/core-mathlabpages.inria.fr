const c99names = [
    'acos', 'acosh', 'asin', 'asinh', 'atan2', 'atan', 'atanh',
    'cbrt', 'cos', 'cosh', 'erfc', 'erf', 'exp2', 'exp', 'expm1', 'hypot',
    'lgamma','log10', 'log1p', 'log2', 'log', 'pow', 'sin', 'sinh', 'tan', 'tanh','tgamma'];
const c23names = [
    'acospi', 'asinpi', 'atan2pi', 'atanpi', 'cospi', 'exp10', 'exp10m1',
    'exp2m1', 'log10', 'log10p1', 'log2p1', 'rsqrt', 'sinpi', 'tanpi'];

const colCM = 'rgb(255, 99, 132)', colGNU = 'rgb(75, 192, 192)', colICX='rgb(54, 162, 235)', colLLVM = 'rgb(255, 153, 255)';

let opts = new Map();
opts.set('lib0', {title:'CORE-MATH 55c872b', col:colCM});
opts.set('lib1', {title:'GNU libc 2.37 [>0.5 ulp]', col:colGNU});
opts.set('lib2', {title:'icx 2023.2.0 [>0.5 ulp]', col:colICX});
opts.set('lib3', {title:'LLVM d099dbb', col:colLLVM});

let fn = new Array(''), dt = new Array(''), libs = ['lib0', 'lib1', 'lib2', 'lib3'];
Chart.defaults.font.size = 18;
let chart;
Promise.all([
    loadata("throughput.out").then(v => filldt(v, 'thr')),
    loadata("latency.out").then(v => filldt(v, 'lat'))
]).then(
    function(){
	chart = new Chart(document.getElementById('perf'), {type:'bar', data: selectData(dt, 'c99f', 'thr', libs), options: setOption('')});
    }
);

document.querySelectorAll('input[name="lib"]').forEach(box => box.addEventListener('change', showSelected));
document.querySelector('#funSet').addEventListener('change', showSelected);
document.querySelector('#perfType').addEventListener('change', showSelected);

function showSelected() {
    let l = document.querySelectorAll('input[name="lib"]');
    libs = [];
    for(let i=0;i<l.length;i++){
	if(l[i].checked) libs.push(l[i].value);
    }

    let fs = document.getElementById('funSet'), pt = document.getElementById('perfType');
    chart.type = 'bar';
    chart.data = selectData(dt, fs.options[fs.selectedIndex].value, pt.options[pt.selectedIndex].value, libs);
    chart.options = setOption('');
    chart.update();
}

function setdt(t, d, cl) {
    return {hidden:false, label: t, data: d, borderWidth: 1, borderColor: cl, backgroundColor: cl};
}

function matchType(fn, type){
    if(type=='c99f'){
	return matchName(fn, 'float', c99names);
    } else if(type=='c99d'){
	return matchName(fn, 'double', c99names);
    } else if(type=='c99l'){
	return matchName(fn, 'long double', c99names);
    } else if(type=='c23f'){
	return matchName(fn, 'float', c23names);
    } else if(type=='c23d'){
	return matchName(fn, 'double', c23names);
    }
}

function matchName(fn, type, names){
    for(i=0; i<names.length; i++){
	let n = names[i];
	if(type == 'double'){
	    if (fn == n) return true;
	} else if(type == 'float'){
	    n += 'f';
	    if (fn == n) return true;
	} else if(type == 'long double'){
	    n += 'l';
	    if (fn == n) return true;
	} else if(type == 'quad'){
	    n += 'q';
	    if (fn == n) return true;
	}
    }
    return false;
}

function selectData(dt, type, lat, libs){
    let fns = new Array(''), dts = new Array(''), nms = new Map();
    for(let i=0; i<dt.length; i++)
	if(matchType(dt[i].name, type)) nms.set(dt[i].name,'');
    let iv = 0; for (const x of nms.keys()) fns[iv++] = x;
    for(let j=0;j<libs.length;j++){
	dts[j] = new Array('');
	iv = 0;
	for(let i=0; i<dt.length; i++){
	    if(dt[i].mtype == lat && dt[i].lib == libs[j] && nms.has(dt[i].name)){
		dts[j][iv++] = dt[i].value;
	    }
	}
    }
    let ds = [];
    for(let j=0;j<libs.length;j++){
	let t = opts.get(libs[j]);
	ds[j] = setdt(t.title, dts[j], t.col);
    }
    return {labels: fns, datasets: ds};
}

function setOption(mytitle){
    return {aspectRatio: 4, plugins: { title: { display: true, text: mytitle}}, scales: { y: { beginAtZero: true}}};
}

function filldt(v, mtype){
    let a = v.split('\n');
    for(let i=0; i<a.length; i++){
	let b = a[i].split(' ');
	let fname = b[0];
	for(let j=1; j<b.length; j++){
	    let k = Math.round(10*Number(b[j]))/10; if(k<0) k = 0;
	    dt.push({name:fname, mtype:mtype, value:k, lib:'lib' + (j-1)});
	}
    }
}

async function loadata(a){
    let u = await fetch(a);
    let v = await u.text();
    return v;
}
