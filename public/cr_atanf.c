/* Correctly-rounded arc-tangent function for binary32.

Copyright (c) 2022 Paul Zimmermann, INRIA.

This file is part of the CORE-MATH project
(https://core-math.gitlabpages.inria.fr/).

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE. */

/* References:
   [1] IA-64 and Elementary Functions, Peter Markstein,
       Hewlett-Packard Professional Books, 2000, Chapter 15. */

/* Notes:
 * this function does not deal with NaN, +Inf or -Inf, and it does not deal
   properly with exceptions nor the inexact flag;
 * the 'main' code tests exhaustively all binary32 values, with -rndn by
   default (rounding to nearest), -rndz (towards zero), -rndu (upwards)
   or -rndd (downwards);
 * tested on x86-64-linux with gcc 10.2.1, with and without -march=native.
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
#include <fenv.h>
#include <mpfr.h>
#include <omp.h>

#define N 32

/* A[k] is an approximation of k*pi/2/N for 0 <= k < N */
static const double A[N]= {
  0x0p+0, 0x1.921fb54442d18p-5, 0x1.921fb54442d18p-4, 
  0x1.2d97c7f3321d2p-3, 0x1.921fb54442d18p-3, 0x1.f6a7a2955385ep-3, 
  0x1.2d97c7f3321d2p-2, 0x1.5fdbbe9bba775p-2, 0x1.921fb54442d18p-2, 
  0x1.c463abeccb2bbp-2, 0x1.f6a7a2955385ep-2, 0x1.1475cc9eedf01p-1, 
  0x1.2d97c7f3321d2p-1, 0x1.46b9c347764a4p-1, 0x1.5fdbbe9bba775p-1, 
  0x1.78fdb9effea47p-1, 0x1.921fb54442d18p-1, 0x1.ab41b09886feap-1, 
  0x1.c463abeccb2bbp-1, 0x1.dd85a7410f58dp-1, 0x1.f6a7a2955385ep-1, 
  0x1.07e4cef4cbd98p+0, 0x1.1475cc9eedf01p+0, 0x1.2106ca4910069p+0, 
  0x1.2d97c7f3321d2p+0, 0x1.3a28c59d5433bp+0, 0x1.46b9c347764a4p+0, 
  0x1.534ac0f19860cp+0, 0x1.5fdbbe9bba775p+0, 0x1.6c6cbc45dc8dep+0, 
  0x1.78fdb9effea47p+0, 0x1.858eb79a20bbp+0, 
};

/* S[k] is an approximation of sin(k*pi/2/N) for 0 <= k < N */
static const double S[N]= {
  0x0p+0, 0x1.91f65f10dd814p-5, 0x1.917a6bc29b42cp-4, 
  0x1.2c8106e8e613ap-3, 0x1.8f8b83c69a60bp-3, 0x1.f19f97b215f1bp-3, 
  0x1.294062ed59f06p-2, 0x1.58f9a75ab1fddp-2, 0x1.87de2a6aea963p-2, 
  0x1.b5d1009e15ccp-2, 0x1.e2b5d3806f63bp-2, 0x1.073879922ffeep-1, 
  0x1.1c73b39ae68c8p-1, 0x1.30ff7fce17035p-1, 0x1.44cf325091dd6p-1, 
  0x1.57d69348cecap-1, 0x1.6a09e667f3bcdp-1, 0x1.7b5df226aafafp-1, 
  0x1.8bc806b151741p-1, 0x1.9b3e047f38741p-1, 0x1.a9b66290ea1a3p-1, 
  0x1.b728345196e3ep-1, 0x1.c38b2f180bdb1p-1, 0x1.ced7af43cc773p-1, 
  0x1.d906bcf328d46p-1, 0x1.e212104f686e5p-1, 0x1.e9f4156c62ddap-1, 
  0x1.f0a7efb9230d7p-1, 0x1.f6297cff75cbp-1, 0x1.fa7557f08a517p-1, 
  0x1.fd88da3d12526p-1, 0x1.ff621e3796d7ep-1
};

/* C[k] is an approximation of cos(k*pi/2/N) for 0 <= k < N */
static const double C[N]= {
  0x1p+0, 0x1.ff621e3796d7ep-1, 0x1.fd88da3d12526p-1, 
  0x1.fa7557f08a517p-1, 0x1.f6297cff75cbp-1, 0x1.f0a7efb9230d7p-1, 
  0x1.e9f4156c62ddap-1, 0x1.e212104f686e5p-1, 0x1.d906bcf328d46p-1, 
  0x1.ced7af43cc773p-1, 0x1.c38b2f180bdb1p-1, 0x1.b728345196e3ep-1, 
  0x1.a9b66290ea1a3p-1, 0x1.9b3e047f38741p-1, 0x1.8bc806b151741p-1, 
  0x1.7b5df226aafafp-1, 0x1.6a09e667f3bcdp-1, 0x1.57d69348cecap-1, 
  0x1.44cf325091dd6p-1, 0x1.30ff7fce17035p-1, 0x1.1c73b39ae68c8p-1, 
  0x1.073879922ffeep-1, 0x1.e2b5d3806f63bp-2, 0x1.b5d1009e15ccp-2, 
  0x1.87de2a6aea963p-2, 0x1.58f9a75ab1fddp-2, 0x1.294062ed59f06p-2, 
  0x1.f19f97b215f1bp-3, 0x1.8f8b83c69a60bp-3, 0x1.2c8106e8e613ap-3, 
  0x1.917a6bc29b42cp-4, 0x1.91f65f10dd814p-5, 
};

/* T[k] is an approximation of tan(k*pi/2/N) for 0 <= k < N */
static const double T[N]= {
  0x0p+0, 0x1.927278a3b1162p-5, 0x1.936bb8c5b2da2p-4, 
  0x1.2fcac73a6064p-3, 0x1.975f5e0553158p-3, 0x1.007fa758626aep-2, 
  0x1.36a08355c63dcp-2, 0x1.6e649f7d78649p-2, 0x1.a827999fcef32p-2, 
  0x1.e450e0d273e7ap-2, 0x1.11ab7190834ecp-1, 0x1.32e1889047ffdp-1, 
  0x1.561b82ab7f99p-1, 0x1.7bb99ed2990cfp-1, 0x1.a43002ae4285p-1, 
  0x1.d00cbc7384d2ep-1, 0x1p+0, 0x1.1a73d55278c4bp+0, 
  0x1.37efd8d87607ep+0, 0x1.592d11142fa55p+0, 0x1.7f218e25a7461p+0, 
  0x1.ab1c35d8a74eap+0, 0x1.def13b73c1406p+0, 0x1.0ea21d716fbf7p1, 
  0x1.3504f333f9de6p1, 0x1.65bc6cc825147p1, 0x1.a5f59e90600ddp1, 
  0x1.ff01305ecd8dcp1, 0x1.41bfee2424771p2, 0x1.af73f4ca3310fp2, 
  0x1.44e6c595afdccp3, 0x1.45affed201b55p+4, 
};

static inline uint32_t
asuint (float f)
{
  union
  {
    float f;
    uint32_t i;
  } u = {f};
  return u.i;
}

float
cr_atanf (float x)
{
  /* deal here with NaN, +Inf and -Inf */

  uint32_t ix = asuint (x);

  double absx = fabsf (x), y;

  /* now |x| <= 1 */
  
  /* For tiny x, atan(x) ~ x - x^3/3. By exhaustive search, we can check
     that for all rounding modes, for |x| < 0x1.713746p-12, rounding
     x - x/2^25 gives the correct rounding. It suffices to search for
     rounding to nearest. */
  if (absx < 0x1.713746p-12)
  {
    y = (double) x;
    y = y - y * 0x1p-25;
    return (float) y;
  }

  /* exceptional cases */
  float hi, lo;
  switch (ix)
  {
  case 0x3d8d6b23: /* x = 0x1.1ad646p-4, 30 identical bits after round bit */
  case 0xbd8d6b23: /* x = -0x1.1ad646p-4, 30 identical bits after round bit */
    hi = 0x1.1a6386p-4f;
    lo = -0x1.fffffep-29f;
    /* need -frounding-math here so that rounding is done at runtime and not
       at compile time */
    return (x > 0) ? hi + lo : -hi - lo;
  case 0x45cc6129: /* x = 0x1.98c252p+12, 28 identical bits after round bit */
  case 0xc5cc6129: /* x = -0x1.98c252p+12, 28 identical bits after round bit */
    hi = 0x1.9215bp+0f;
    lo = -0x1.069c58p-53f;
    return (x > 0) ? hi + lo : -hi - lo;
  case 0xc7b8d9fa: /* x = -0x1.71b3f4p+16, 28 identical bits after round bit */
    hi = -0x1.921f04p+0f;
    lo = 0x1.4d3ffcp-53f;
    return hi + lo;
  case 0xbfeefcfb: /* x = -0x1.ddf9f6p+0, 29 identical bits after round bit */
    hi = -0x1.143ec4p+0f;
    lo = -0x1.5e8582p-54f;
    return hi + lo;
  }

  /* Argument reduction: split [0,1] into 32 sub-ranges, and use a
     degree-9 polynomial on each (cf Table 14.1). */
  double g;
  /* We want |x| near tan(k*pi/(2N)) thus k near atan|x|*2N/pi.
     Use dichotomy. */
  int k = 0, l = N;
  for (int i = 0; i < 5; i++)
  {
    int m = (k + l) / 2;
    if (absx < T[m])
      l = m;
    else
      k = m;
  }

  /* Now T[k] <= |x| < T[k+1] thus k*pi/2/N <= atan |x| <= (k+1)*pi/2/N.
     Use formula (15.2.1) from [1]. */
  g = (absx * C[k] - S[k]) / (C[k] + absx * S[k]);
  /* let y = atan |x| = A[k] + d, then 0 <= d <= pi/2/N, and g approximates
     tan(d) */
  /* degree-9 polynomial generated by Sollya (file atanf.sollya) */
  static const double P[5] = { 0x1.fffffffffffffp-1, -0x1.5555555551e4dp-2,
    0x1.9999995fe1b29p-3, -0x1.24917cdfb8f7ep-3, 0x1.c49954dfcb943p-4 };
  double gg = g * g;
  y = P[4];
  y = P[3] + gg * y;
  y = P[2] + gg * y;
  y = P[1] + gg * y;
  y = P[0] + gg * y;
  y = g * y;         /* y approximates d */
  y = A[k] + y;
  return (float) ((x >= 0) ? y : -y);
}

/* the code below is to check correctness by exhaustive search */

int rnd1[] = { FE_TONEAREST, FE_TOWARDZERO, FE_UPWARD, FE_DOWNWARD };
mpfr_rnd_t rnd2[] = { MPFR_RNDN, MPFR_RNDZ, MPFR_RNDU, MPFR_RNDD };

mpfr_rnd_t rnd = MPFR_RNDN; /* default is to nearest */

typedef union { uint32_t n; float x; } union_t;

#define BIAS 127

/* code from MPFR */
float
ref_atan (float x)
{
  mpfr_t y;
  mpfr_init2 (y, 24);
  mpfr_set_flt (y, x, MPFR_RNDN);
  int inex = mpfr_atan (y, y, rnd2[rnd]);
  mpfr_subnormalize (y, inex, rnd2[rnd]);
  float ret = mpfr_get_flt (y, MPFR_RNDN);
  mpfr_clear (y);
  return ret;
}

float
asfloat (uint32_t n)
{
  union_t u;
  u.n = n;
  return u.x;
}

void
doit (uint32_t n)
{
  float x, y, z;
  mpfr_set_emin (-148);
  mpfr_set_emax (128);
  x = asfloat (n);
  y = ref_atan (x);
  fesetround (rnd1[rnd]);
  z = cr_atanf (x);
  if (y != z)
  {
    printf ("FAIL x=%a ref=%a y=%a\n", x, y, z);
    fflush (stdout);
    exit (1);
  }
}

int
main (int argc, char *argv[])
{
  while (argc >= 2)
    {
      if (strcmp (argv[1], "-rndn") == 0)
        {
          rnd = (mpfr_rnd_t) 0;
          argc --;
          argv ++;
        }
      else if (strcmp (argv[1], "-rndz") == 0)
        {
          rnd = (mpfr_rnd_t) 1;
          argc --;
          argv ++;
        }
      else if (strcmp (argv[1], "-rndu") == 0)
        {
          rnd = (mpfr_rnd_t) 2;
          argc --;
          argv ++;
        }
      else if (strcmp (argv[1], "-rndd") == 0)
        {
          rnd = (mpfr_rnd_t) 3;
          argc --;
          argv ++;
        }
      else
        {
          fprintf (stderr, "Error, unknown option %s\n", argv[1]);
          exit (1);
        }
    }

  uint32_t nmin = asuint (0x1p-149f), nmax = asuint (0x1.fffffep127f);
#pragma omp parallel for
  for (uint32_t n = nmin; n <= nmax; n++)
  {
    doit (n);
    doit (n | 0x80000000);
  }
  printf ("all ok\n");
  return 0;
}


